cmake_minimum_required(VERSION 3.5)

project(clash LANGUAGES C)

add_executable(clash clash.c plist.c plist.h)
