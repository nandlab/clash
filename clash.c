#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <limits.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <sys/wait.h>
#include "plist.h"

#define MAX_LINE_LEN 1337

static char* auto_get_cwd() {
    size_t size = 8;
    char *cwd = NULL;
    for (; 1; size *= 2) {
        char *new_cwd = (char*) realloc(cwd, size);
        if (!new_cwd) {
            free(cwd);
            return NULL;
        }
        cwd = new_cwd;

        if (getcwd(cwd, size))
            break;

        if (errno != ERANGE) {
            free(cwd);
            return NULL;
        }
    }
    return cwd;
}

// Print the command prompt.
// Return values:
// 0: success
// -1: allocation failure
// -2: stdout error
static int print_prompt() {
    char *cwd = auto_get_cwd();
    int ec = 0;
    if (!cwd) {
        ec = -1;
        goto myexit;
    }
    if (printf("%s: ", cwd) < 0) {
        ec = -2;
        goto myexit;
    }
    if (fflush(stdout) == EOF) {
        ec = -2;
        goto myexit;
    }
myexit:
    free(cwd);
    return ec;
}

// Returns the array of tokens on success, *ntokens is set to the number of tokens.
// The array will have a NULL terminator at index *ntokens.
// The returned array should be freed by the user.
// On allocation failure, NULL is returned and *ntokens is undefined.
static char** tokenize(char* line, size_t* ntokens) {
    *ntokens = 0;

    size_t size = 8;
    char **tokens = (char**) malloc(sizeof(char*) * size);
    if (!tokens)
        return NULL;

    tokens[0] = strtok(line, " \t\n");

    if (!tokens[0])
        return tokens;

    *ntokens = 1;
    while( (tokens[*ntokens] = strtok(NULL, " \t\n")) != NULL ) {
        (*ntokens)++;
        if (*ntokens >= size) {
            size *= 2;
            char **new_tokens = (char**) realloc(tokens, sizeof(char*) * size);
            if (!new_tokens) {
                free(tokens);
                return NULL;
            }
            tokens = new_tokens;
        }
    }
    return tokens;
}

// Returns zero on success and non-zero on stdout error.
static int print_wstatus(const char* commandline, int wstatus) {
    if (WIFEXITED(wstatus)) {
        if (printf("Exitstatus [%s] = %d\n", commandline, WEXITSTATUS(wstatus)) < 0)
            return -1;
    }
    else {
        if (printf("[%s] did not terminate normally.\n", commandline) < 0)
            return -1;
    }
    return 0;
}

/****** Begin: Callbacks for the walkList function ******/

static int wait_error = 0;

static pid_t bg_pid;
static const char* bg_commandline;
static int bg_wstatus;

static bool builtin_stdout_error = false;

// commandline is assumed to stay valid after walkList returns.
static int walk_get_first_terminated_process(pid_t pid, const char *commandline) {
    (void)commandline;
    int wstatus;
    pid_t ret = waitpid(pid, &wstatus, WNOHANG);
    if (ret == -1) {
        wait_error = 1;
        return -1;
    }
    if (ret) {
        bg_pid = ret;
        bg_commandline = commandline;
        bg_wstatus = wstatus;

        // Stop walking the list when the first terminated process is found.
        return -1;
    }
    return 0;
}

static int walk_get_first_process(pid_t pid, const char *commandline) {
    (void)commandline;
    bg_pid = pid;
    return -1;
}

static int walk_print_process(pid_t pid, const char *commandline) {
    if (printf("%d: %s\n", pid, commandline) < 0) {
        builtin_stdout_error = true;
        return -1;
    }
    return 0;
}

/******* End: Callbacks for the walkList function *******/


static int clash_builtin_cd(int argc, char *argv[]) {
    const char *target_dir;
    if (argc <= 1) {
        target_dir = getenv("HOME");
        if (!target_dir) {
            fputs("Error: Cannot get HOME variable.\n", stderr);
            return 1;
        }
    }
    else {
        target_dir = argv[1];
    }
    if (chdir(target_dir) == -1) {
        perror("chdir");
        return 2;
    }
    return 0;
}



static int clash_builtin_jobs(int argc, char *argv[]) {
    (void)argc;
    (void)argv;
    walkList(walk_print_process);
    return builtin_stdout_error ? 1 : 0;
}

static bool clash_exit = false;

static int clash_builtin_exit(int argc, char *argv[]) {
    (void)argc;
    (void)argv;
    clash_exit = true;
    return 0;
}

struct clash_builtin {
    const char *name;
    int (*const fptr)(int argc, char *argv[]);
};

const static struct clash_builtin clash_builtins[] = {
    {"cd", clash_builtin_cd},
    {"jobs", clash_builtin_jobs},
    {"exit", clash_builtin_exit},
    {NULL, NULL}
};

#define NUM_CLASH_BUILTINS (sizeof(clash_builtins) / sizeof(*clash_builtins));

// Interpret the command line.
// Finally, print the exit status of the terminated processes.
// Return values:
// 0: success
// -1: allocation failure
// -2: fork error
// -3: exec error (in child)
// -4: wait error
// -5: stdout error
static int interpret_command_line(char* command_line, size_t command_line_len) {
    if (command_line_len > 0 && command_line[command_line_len - 1] == '\n') {
        command_line[command_line_len - 1] = '\0';
        command_line_len--;
    }

    size_t ntokens;

    char *command_line_bak = strdup(command_line);
    char **tokens = tokenize(command_line, &ntokens);

    if (!tokens) {
        free(command_line_bak);
        return -1;
    }

    // If the last token is "&", the process is started in the background.
    bool start_in_background = ntokens > 0 && !strcmp(tokens[ntokens - 1], "&");

    if (start_in_background) {
        // Remove the last "&" token
        tokens[ntokens - 1] = (char*) NULL;
        ntokens--;
    }

    if (!ntokens) {
        free(tokens);
        free(command_line_bak);
    }
    else {
        // Check if the command is the shell builtin.
        bool is_builtin = false;
        const struct clash_builtin* builtin = clash_builtins;
        while (builtin->name) {
            if (!strcmp(tokens[0], builtin->name)) {
                is_builtin = true;
                builtin->fptr(ntokens, tokens);
                free(tokens);
                free(command_line_bak);
                if (builtin_stdout_error) {
                    return -5;
                }
                break;
            }
            builtin++;
        }

        if (!is_builtin) {
            pid_t pid = fork();

            if (pid == -1) {
                free(tokens);
                free(command_line_bak);
                return -2;
            }

            if (!pid) {
                // In child process
                if (execvp(tokens[0], tokens) == -1) {
                    free(tokens);
                    free(command_line_bak);
                    return -3;
                }
            }

            free(tokens);

            if (start_in_background) {
                if (insertElement(pid, command_line_bak) == -2) {
                    free(command_line_bak);
                    return -1;
                }
            }
            else {
                int wstatus;
                if (waitpid(pid, &wstatus, 0) == -1) {
                    free(command_line_bak);
                    return -4;
                }

                if (print_wstatus(command_line_bak, wstatus)) {
                    free(command_line_bak);
                    return -5;
                }
            }
            free(command_line_bak);
        }
    }

    if (!clash_exit) {
        bool print_error = false;
        while (
            bg_commandline = NULL,
            bg_pid = 0,
            walkList(walk_get_first_terminated_process),

            !wait_error && bg_pid
        ) {
            if (print_wstatus(bg_commandline, bg_wstatus)) {
                print_error = true;
                break;
            }
            char dummy;
            removeElement(bg_pid, &dummy, 1);
        }
        if (wait_error)
            return -4;
        if (print_error)
            return -5;
    }
    return 0;
}

int main()
{
    char *line = NULL;
    size_t linesize = 0;
    ssize_t linelen = 0;
    int print_err = 0;
    int interpret_err = 0;
    while (!clash_exit && !(print_err = print_prompt()) && (linelen = getline(&line, &linesize, stdin)) != -1) {
        if (linelen > MAX_LINE_LEN) {
            fputs("Warning: Line rejected because it is too long!\n", stderr);
            continue;
        }
        if ((interpret_err = interpret_command_line(line, linelen)) != 0)
            break;
    }

    if (print_err) {
        perror("print_prompt");
        return 1;
    }
    if (linelen == -1 && ferror(stdin)) {
        perror("getline");
        return 2;
    }
    if (interpret_err) {
        perror("interpret_command_line");
        return 3;
    }

    // Free allocated resources
    free(line);
    while (bg_pid = 0, walkList(walk_get_first_process), bg_pid) {
        char dummy;
        removeElement(bg_pid, &dummy, 1);
    }

    if (feof(stdin)) {
        if (puts("") == EOF) {
            perror("puts");
            return 1;
        }
    }
    return 0;
}
