.PHONY: all clean

CC  := gcc
RM  := rm
SED := sed

CPPFLAGS := -D_XOPEN_SOURCE=700
CFLAGS   := -std=c11 -pedantic -Wall -Werror
LDFLAGS  :=
LDLIBS   :=

OUT  := clash
SRC  := $(wildcard *.c)
BASE := $(basename $(SRC))
OBJ  := $(addsuffix .o,$(BASE))
DEP  := $(addsuffix .d,$(BASE))

all: $(OUT)

ifneq ($(MAKECMDGOALS),clean)
include $(DEP)
endif

$(OUT): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)

%.o: %.c
	$(CC) -o $@ $< -c $(CPPFLAGS) $(CFLAGS)

%.d: %.c
	$(CC) -M $(CPPFLAGS) $(CFLAGS) $< | $(SED) 's,\($*\)\.o[ :]*,\1.o $@ : ,g' > $@

clean:
	$(RM) -f $(OUT) $(OBJ) $(DEP)
